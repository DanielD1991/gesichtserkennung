1.1 Elemente der Arbeit
Eine wissenschaftliche Arbeit besteht aus mehreren Teilen, die in folgender Reihenfolge anzuordnen sind:1
  Titelblatt
  Inhaltsverzeichnis
  Abbildungs-, Tabellen- und Abkürzungsverzeichnis
  ggf. Formel- und Symbolverzeichnis
  Sperrvermerk
  Textteil2
  ggf. Anhang und Anlagen
  Literaturverzeichnis
  ggf. Rechtsprechungsverzeichnis
  ggf. Quellenverzeichnis
  Eidesstattliche Erklärung

 (Vgl. Theisen, M. R., Wissenschaftliches Arbeiten, 2013, S. 194–210.
 An dieser Stelle sei angemerkt, dass bei Untergliederungen einzelner Kapitel jeweils eine kurze Einführung 
 (2–4 Sätze) in das Kapitel erfolgt. Zum Beispiel hat das dritte Kapitel vier Unterka- pitel, sodass zwischen 
 3. und 3.1 eine kurze Einführung in das Kapitel erfolgt. Vgl. Theisen, M. R., Wissenschaftliches Arbeiten, 
 2013, S. 147–153. Dies gilt nicht für diesen Leitfaden, da es sich nicht um eine wissenschaftliche Arbeit 
 handelt.)
 
 
1.2 Layout (siehe 01b_Leitfaden...)
  
  DIN A4 Hochformat
  
 Schriftgrad und Zeilenabstand:
  -Zeilenabstand im Text: 1,5-zeilig, Schriftgrad 12 pt (Times New Roman)
  -(ungefähr 37 Zeilen mit je ungefähr 60 Zeichen bzw. 2.200 Zeichen pro Seite erreicht werden.)
  -Abstände im Text vor einem Absatz sind mit 0 pt und nach einem Absatz bei 6 pt als Grundeinstellung
  -Der Zeilenabstand bei Überschriften inkl. für Beschriftungen bei Abbildungen, Tabellen und Formeln soll 
   vor einem Absatz 12 pt und nach einem Absatz 6 pt betragen.5
  -Zeilenabstand in Fußnoten einzeilig, Schriftgrad 10 pt (Times New Roman) oder 9,5 pt (Arial).6
  -Textteile sind im Blocksatz zu halten
  -Eigennamen können kursiv oder in Großbuchstaben verwendet werden
  -Die automatische Silbentrennung ist einzuschalten
  -Fußnoten sind durch einen Strich vom Textbaustein optisch zu trennen
  -Fußnoten11, Abbildungen, Tabellen o. ä. werden fortlaufend nummeriert
  -Alle Überschriften im Text sind linksbündig am Rand des entsprechenden Ordnungspunkts und werden fett hervorgehoben

 Der Abstand des Textes vom Seitenrand enthält folgende Richtwerte:
  -Oben: 4 cm (zusätzliche Einstellung der Kopfzeile von oben 2 cm)
  -Unten: 2 cm (zusätzliche Einstellung der Fußzeile von unten 2 cm)
  -Links: 4 cm
  -Rechts: 2 cm
  -Mit Ausnahme des Titelblatts sind alle Seiten der Arbeit fortlaufend innerhalb der oberen Kopfzeile zu nummerieren
  -Seitenzahl 1 cm rechts von der geometrischen Mitte, welches durch das Einfügen der Seitenzahl automatisch durchgeführt wird
  -beginnend mit der ersten Seite des Inhaltsverzeichnisses, sämtliche Verzeichnisse vor dem Textteil mit römischen Ziffern fortlaufend durchzunummerieren
  -dass das Titelblatt mitgezählt wird. Es erhält jedoch keine Seitenzahl. as Inhaltsverzeichnis erhält als erste Seitenzahl mithin die römische Zahl II
  -Beginnend mit der ersten Seite des Text- teils werden alle Seiten – mit Ausnahme der eidesstattlichen Erklärung16 – mit arabischen Ziffern fortlaufend nummeriert
  
 Folgender Mindest- und Höchstumfang ist vorgesehen:
  -Seminararbeiten/Assignments: In Abhängigkeit von der jeweiligen Modulbe- schreibung z. B. 4.000 Worte.
  -40–60 Seiten (Bachelor-Thesis); 60–80 Seiten (Master-Thesis).